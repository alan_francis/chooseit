package com.neo.chooseit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alan-1554 on 04/09/15.
 */
class ChooseIt {

    private static ArrayList<String> keys;
    private static ArrayList<String> values;
    private static Properties dictionary;

    public ChooseIt(String filename){
        keys = new ArrayList<>();
        values = new ArrayList<>();
        dictionary = getDictionary(filename);

        Enumeration enu = dictionary.keys();
        while(enu.hasMoreElements()){
            keys.add(enu.nextElement().toString());
        }
        values.addAll(keys.stream()
                          .map(dictionary::getProperty)
                          .collect(Collectors.toList()));
    }

    public static void log(String format, Object ...values){
        for (Object value : values) {
            System.out.print(value.toString() + " ");
        }
        System.out.println();
    }

    private Properties getDictionary(String filename){
        Properties prop = new Properties();

        try{
            prop.load(getClass().getResourceAsStream(filename));
        }catch (FileNotFoundException fe){
            fe.printStackTrace();
        }catch (IOException ie){
            ie.printStackTrace();
        }
        log(null,"All properties:", prop.toString());
        return prop;
    }

    public static ArrayList<String> chooseQuestions(int limit){
        return chooseRandom(keys, limit);
    }

    public static ArrayList<String> chooseRandom(List<String> someList, int howMany){
        List<String> keys = someList.subList(0,someList.size());
        ArrayList<String> chosenKeys = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<howMany;i++){
            int size = keys.size();
            if(size >0) {
                String key = keys.get(random.nextInt(size));
                if(chosenKeys.contains(key)){
                    i--;
                }else{
                    chosenKeys.add(key);
                }
            }
        }
        log(null,"Chosen at random:", chosenKeys);
        return chosenKeys;
    }

    public static ArrayList<String> getChoicesForQuestion(String question, int howMany){

        List<String> wrongAnswers = values.subList(0, values.size());
        wrongAnswers.remove(dictionary.getProperty(question));
        ArrayList<String> answers = chooseRandom(wrongAnswers, howMany-1);
        answers.add(dictionary.getProperty(question));
        log(null,"QuestionBean:", question, "\nAnswers:", answers);
        return answers;
    }

    public void quizCMD(HashMap questionairre){
        ArrayList<String> correctQuestions = new ArrayList<>();
        log(null, "Welcome to the game of choose..");
        log(null, "Start the game?? (y/n)");
        Scanner scan = new Scanner(System.in);
        String start = scan.nextLine();
        if(start.equalsIgnoreCase("y")){
            Iterator it = questionairre.entrySet().iterator();
            int i=1;
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                String question = pair.getKey().toString();
                log(null, i+". What is the meaning of the word:",question,"?");
                ArrayList answers = (ArrayList)pair.getValue();
                Collections.shuffle(answers);
                for(int j=1;j<=answers.size();j++){
                    log(null, j,":",answers.get(j-1));
                }
                System.out.print("Enter your answer:  ");
                String answer = scan.nextLine();
                String chosenans = answers.get(Integer.parseInt(answer)-1).toString();
                if(dictionary.getProperty(question).equalsIgnoreCase(chosenans)){
                    correctQuestions.add(question);
                }
            }
            if(correctQuestions.size()>0) {
                log(null, "Congrats you got", correctQuestions.size(), "questions right!!");
                log(null, "Questions you got right were:");
                correctQuestions.forEach(q -> log(null, q));
            }else{
                log(null, "You got nothing right!, You suck at this!");
            }

        }else{
            log(null, "See you later!!!");
            System.exit(0);
        }
    }

    public void newquizCMD(List<QuestionBean> questionBeanList, int howMany){
        ArrayList<String> correctQuestions = new ArrayList<>();
        Integer score = 0;
        log(null, "Welcome to the new and improved game of choose..");
        log(null, "Start the game?? (y/n)");
        Scanner scan = new Scanner(System.in);
        String start = scan.nextLine();
        if(start.equalsIgnoreCase("y")){
            Collections.shuffle(questionBeanList);
            int counter = 0;
            for(QuestionBean questionBean : questionBeanList){
                if (counter==howMany){
                    break;
                }
                log(null, questionBean.getQuestion());
                List answers = questionBean.getAnswers();
                Collections.shuffle(answers);
                for(int j=1;j<=answers.size();j++){
                    log(null, j,":",answers.get(j-1));
                }
                System.out.print("Enter your answer:  ");
                String answer = scan.nextLine();
                try{
                    Integer.parseInt(answer);
                }
                catch (NumberFormatException nfe){
                    log(null, "Please enter a number not a text");
                    answer = scan.nextLine();//TODO: based on good faith, reimplement using recursion, with a warning at 3 and break at 5!
                }
                String chosenans = answers.get(Integer.parseInt(answer)-1).toString();
                if(questionBean.getCorrectAnswer().equals(chosenans)){
                    score += (Integer) questionBean.getScore();
                    correctQuestions.add(questionBean.getQuestion());
                }
                counter++;
            }
            if(correctQuestions.size()>0) {
                log(null, "Congrats you got", correctQuestions.size(), "questions right out of "+howMany+"!!");
                log(null, "Questions you got right were:");
                for (String q : correctQuestions) {
                    log(null, q);
                }
            }else{
                log(null, "You got nothing right!, You suck at this!");
            }
        }else{
            log(null, "See you later!!!");
            System.exit(0);
        }
    }

    public static void main(String args[]){
        int howMany = 2;
        ChooseIt c = new ChooseIt("dictionary.properties");
        ParseJson p = new ParseJson();
        boolean useJson = Boolean.parseBoolean(System.getProperty("use.json", "false"));
        List<QuestionBean> questionBeanList = new ArrayList<>();

        if (useJson){
            questionBeanList = p.parseJson("questions.json");
        }else {
            questionBeanList = new ParseDB().parseDB(null, howMany);
        }

        log(null, questionBeanList);
        c.newquizCMD(questionBeanList, howMany);

//        ArrayList<String> questions = chooseQuestions(2);
//        HashMap questionairre = new HashMap();
//        for (String question: questions){
//            ArrayList answers = getChoicesForQuestion(question, 4);
//            questionairre.put(question, answers);
//        }
//        c.quizCMD(questionairre);  // Using properties file format.
    }
}
