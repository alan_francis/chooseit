package com.neo.chooseit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by alan-1554 on 08/09/15.
 */
class ParseJson {

    public List<QuestionBean> parseJson(String filename){
        Object obj=JSONValue.parse(new InputStreamReader(getClass().getResourceAsStream(filename)));
        JSONArray array=(JSONArray)obj;
        List<QuestionBean> questionBeanList = new ArrayList<>();

        for (Object object : array){
            QuestionBean questionBean = new QuestionBean();
            JSONObject jsonObject = (JSONObject) object;
            questionBean.setQuestion(jsonObject.get("question").toString());
            questionBean.setCategory(jsonObject.get("category").toString());
            questionBean.setScore(Integer.parseInt(jsonObject.get("score").toString()));
            JSONArray answerArray = (JSONArray)jsonObject.get("answers");
            List<String> answers = new ArrayList<>();
            answerArray.forEach(answer -> answers.add(answer.toString())); //Lambda expressions, learn more!!
            questionBean.setAnswers(answers);
            questionBean.setCorrectAnswer(answers.get(0));
            questionBeanList.add(questionBean);
        }
        ChooseIt.log(null, questionBeanList);
        return questionBeanList;
    }
}
