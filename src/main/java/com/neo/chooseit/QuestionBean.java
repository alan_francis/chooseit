package com.neo.chooseit;

import org.json.simple.JSONObject;

import java.util.List;

/**
 * Created by alan-1554 on 08/09/15.
 */
class QuestionBean {

    private Long questionid;
    private String question;
    private List answers;
    private Number score;
    private String category;
    private String correctAnswer;

    public Long getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Long questionid) {
        this.questionid = questionid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List getAnswers() {
        return answers;
    }

    public void setAnswers(List answers) {
        this.answers = answers;
    }

    public Number getScore() {
        return score;
    }

    public void setScore(Number score) {
        this.score = score;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String toString(){
        return "\nQuestionBean: "+question+
                "\nCategory: "+category+
                "\nScore: "+score+
                "\nAnswers: "+answers+
                "\nCorrect Answer: "+correctAnswer;
    }

    public JSONObject toJSON(){
        return this.toJSON();
    }

}
